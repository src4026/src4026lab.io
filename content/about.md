---
title: "About Me"
date: 2023-12-25T17:12:21+05:30
draft: false
---

Hello! I am `s20`, also known by the aliases `src4026`, and `cosmician` and a warm welcome to a tiny part of my digital cosmos. This website is where I will be showcasing my writings, projects, a collection of my varied interests, and much more.

You can think of me as

> *a peculiar lump of ordered matter changing with respect to time, aspiring to contribute and change the world. Along the journey, he met love, the greatest asset but scarce...*

My foray into the much larger world of the internet began by contributing to the [Minetest](https://github.com/minetest/minetest) [Capture the Flag game by rubenwardy](https://github.com/MT-CTF/capturetheflag). Starting from that moment, I was soon introduced to many things I had previously not known, and after many adventures, here I am.

You can check out my [GitLab](https://gitlab.com/src4026) (maximum of my public projects) and [GitHub](https://github.com/src4026) (contributions and repositories not suitable for GitLab) for my public projects.

I plan to add more to this in the future. Enjoy!
