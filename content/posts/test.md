---
title: "Test"
date: 2023-12-25T21:47:27+05:30
draft: true
cover:
    image: starrynight.png
    alt: "The Starry Night by Vincent van Gogh"
    caption: "The Starry Night by Vincent van Gogh"
tags: ["html", "css"]
categories: ["tech"]
---

# This is heading one
## This is heading two

This is a para.

| Hello   | World     |
|---------|-----------|
| Testing | Something |

1. foo
2. bar
3. barfoo

- Hello
- World

```python
def f(x):
    return x**2

f(2)
```

`x` is a variable.

## Does math work?

$x^2$
$$
x^2+2x+1=0
$$