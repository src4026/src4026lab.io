# [src4026.gitlab.io](https://src4026.gitlab.io)

*Welcome! I see that you have stumbled upon the source code for [my website](https://src4026.gitlab.io).*

Made with love, [Hugo](https://gohugo.io), and the [PaperMod theme](https://github.com/adityatelange/hugo-PaperMod/).